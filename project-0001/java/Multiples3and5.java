
/**
  * Find the sum of all the multiples of 3 or 5 below N.
  *
  * @author Andrew Carr
  */
public class Multiples3and5 {
	public static void main(String[] args ) {
		int limit = 1000;
		if( args.length == 1) {
			limit = Integer.parseInt(args[0]);
		}
		System.out.println( "Determining the sum of the digits that are multiples of 3 and 5 from 0 to " + limit + ", exclusive");

		int sum = 0;
		for( int i = 1; i < limit; i++ ) {
			// Only add the number if it's a multiple of 3 OR 5. If it's both, it should only be added once.
			if( i % 3 == 0 ) {
				sum += i;
			} else if( i % 5 == 0 ) { 
				sum += i;
			}
		}

		System.out.println( "Sum is [" + sum + "]");
	}
}